<?php
/**
 * Manager für Administratoren
 *
 * @author Johannes
 */
class Administration {
    private $database;
    /**
     * DB starten
     */
    public function __construct() {
        require_once 'medoo.php';
        $this->database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
    }
    
    public function createDBs() {
        
    }
}
