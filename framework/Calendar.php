<?php

/**
 * Kalender
 * @author Johannes Witt
 */
class Calendar {
    private $database;
    /**
     * DB starten
     */
    public function __construct() {
        require_once 'medoo.php';
        $this->database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
    }

    /**
     * Ist die Ausgabe des Kalenders
     * @todo Große Spalten machen
     * @todo Bei Doppelklick unteres Formular als InSite-PopUp
     * @todo Layout so ähnlich wie bei Outlook
     * 
     */
    public function draw() {
        $datas = $this->getData();
        echo '<table class="table">
        <tr><th>Von</th><th>Bis</th><th>Betreff</th><th>Ort</th><th>Kommentare</th></tr>';
        if (count($datas) > 0) {
            foreach ($datas as $data) {
                echo '<tr><td>'.date("d.m.Y H:i",$data['start']).'</td><td>'.date("d.m.Y H:i",$data['end']).'</td><td>'.$data['subject'].'</td><td>'.$data['place'].'</td><td>'.$data['comment'].'</td></tr>';
            }
        }
        
        echo '</table>';
    }

    /**
     * Gibt das Formular zum Eintragen von neuen Kalendereinträgen aus
     * @todo All das nach Outlookdesign (BS)
     * @done JavaScript FirstInstance Validierung, nur das anzeigen was geht (JS)
     * @todo Bei Doppelklick in Feld dieses Formular anzeigen
     */
    public function form($severity = false, $message = false) {
        if(!$message == false) {
            switch($severity) {
                case "success":
                    echo '<div class="alert alert-success alert-dismissible" role="alert">'
                        . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        . '<span aria-hidden="true">&times;</span></button>'
                        . $message
                        .'</div>';
                    break;
                case "info":
                    echo '<div class="alert alert-info alert-dismissible" role="alert">'
                        . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        . '<span aria-hidden="true">&times;</span></button>'
                        . $message
                        .'</div>';
                    break;
                case "warning":
                    echo '<div class="alert alert-warning alert-dismissible" role="alert">'
                        . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        . '<span aria-hidden="true">&times;</span></button>'
                        . $message
                        .'</div>';
                    break;
                case "danger":
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">'
                        . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        . '<span aria-hidden="true">&times;</span></button>'
                        . $message
                        .'</div>';
                    break;
                default:
                    echo $message;
                    break;
            }
        }
        require_once 'assets/calendar/form.php';
        $_SESSION['include'] = "dateTime";
        
    }

    /**
     * Interne Abfragefunktion
     * Füllt die Tage mit evtl. Terminen
     */
    private function getData() {
        $returnDatas = $this->database->select("calendar", [
            'subject',
            'place',
            'start',
            'end',
            'allDay',
            'comment',
        ], [
            "USERID" => $_SESSION['id'],
            'ORDER' => [
                'start' => "ASC"
            ],
        ]);
        $error = $this->database->error();
        //echo $error[0];
        return $returnDatas;
    }

    /**
     * Eintragen von Terminen
     * @return boolean
     */
    public function send() {
        if(!empty($_POST['startDate']) and !empty($_POST['endDate']) and !empty($_POST['subject']) and !empty($_POST['place'])) {
            if(!isset($_POST['allDay']) && (empty($_POST['startTime']) || empty($_POST['endTime']))) {
                $_SESSION['message'] = [0 => "info", 1 => "Bitte Zeiten angeben!"];
                return false;
            }
            if(isset($_POST['allDay'])) {
                $allDay = true;
            } else {
                $allDay = false;
            }
            $sdate = strtotime($_POST['startDate']) + strtotime($_POST['startTime']) - strtotime(date("d.m.Y"));
            $start = htmlspecialchars($sdate);
            //$start = date("d.m.Y H:i", $sdate);
            $edate = strtotime($_POST['endDate']) + strtotime($_POST['endTime']) - strtotime(date("d.m.Y"));
            $end = htmlspecialchars($edate);
            //$end = date("d.m.Y H:i", $edate);
            
            if($this->sendSQL($start, $end, $allDay)) {
                $_SESSION['message'] = [0 => "success", 1 => "Termin wurde eingetragen!"];
            } else {
                $_SESSION['message'] = [0 => "danger", 1 => "<strong>Uuuups :(</strong> Aufgrund eines Problems beim Speichern des Termins wurde der Termin nicht eingetragen!"];
                return false;
            }
            
        } else {
            $_SESSION['message'] = [0 => "info", 1 => "Daten fehlen!"];
        }
        
        
    }
    private function sendSQL($start, $end, $allDay){
        $this->database->insert("calendar", [
            'USERID' => $_SESSION['id'],
            'subject' => htmlspecialchars($_POST['subject']),
            'place' => htmlspecialchars($_POST['place']),
            'start' => $start,
            'end' => $end,
            'allDay' => $allDay,
            'comment' => htmlspecialchars($_POST['comment']),   
        ]);
        $error = $this->database->error();
        if(!$error[0] == "00000") {
            return false;
        }
        return true;
        //var_dump($database->debug());
        //var_dump($database->log());
    }

}
