<?php
/**
 * Stellt globale Stundenverarbeitungsmethoden zur Verfügung
 *
 * @author Johannes
 */
class ClassManager {
    /**
     * 
     */
    public function __construct() {
        
    }
    /**
     * 
     */
    public function presence() {
        require_once 'assets/class/presence.php';
    }
    /**
     * 
     */
    public function classbook(){
        require_once 'assets/class/book.php';
        $_SESSION['include'] = "dateWK";
    }
}
