<?php

/**
 * 
 * @author Johannes
 */
class Homework {
    private $database;
    public function __construct() {
        require_once 'medoo.php';
        $this->database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
    }
    public function homeworkTable() {
        $_SESSION['include'] = "dateTime";
        switch ($_SESSION['level']) {
            case 2:
                require_once 'assets/homework/studentTable.php';
                break;
            case 3:
                require_once 'assets/homework/teacherTable.php';
                
                break;
            default:
                break;
        }
    }
}
