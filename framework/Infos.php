<?php
/**
 * Hier kommen Information rein und können für die Anwendung bearbeitet werden
 */
class Infos {
    private $version = "2017.2.1";
    private $hinweis = "Achtung: Diese Seite ist eine Entwicklungsversion. Manches kann unter Umständen nicht funktionieren.";
    
    /**
     * Gibt die derzeitige Version aus
     */
    public function version() {
        echo $this->version;
    }
    /**
     * Gibt Hinweis aus
     */
    public function hinweis() {
        echo $this->hinweis;
    }
}