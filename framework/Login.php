<?php
/**
 * Stellt alle Loginmethoden zur Verfügung
 * 
 * @author Johannes Witt
 * @todo Passwort vergessen
 * @todo Registrierung
 */
class Login {
    /**
     * Loginformular, aber inline
     */
    public function inline($kommentar = "") {
        echo '<div class="container-fluid">
            <div class="col-xs-12">
            <form class="form-inline" method="POST" title="Einloggen" name="einloggen" action="?id=1000">
                <div class="form-group">
                      <label>Anmelden:</label>
                </div>
                    <div class="form-group">
                    
                        <label class="sr-only" for="email">E-Mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail" required="required">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="pass">Passwort</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Passwort" required="required">
                    </div>
                    <button type="submit" class="btn btn-default">Anmelden</button></form>';
        if (!$kommentar == "") { echo $kommentar; }
                echo'</div></div>';
    }
    /**
     * Loginformular normal
     * @param string Einen Komentar unter dem Anmeldeformular ausgeben
     */
    public function out($kommentar = "") {
        echo '<div class="container-fluid">
            <div class="row">
            <div class="hidden-xs col-sm-1 col-md-2 col-lg-3"></div><div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
            <form method="POST" title="Einloggen" name="einloggen" action="?id=1000">
                <div class="form-group">
                      <label>Anmelden:</label>
                </div>
                    <div class="form-group">
                    
                        <label for="email">E-Mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail" required="required">
                    </div>
                    <div class="form-group">
                        <label for="pass">Passwort</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Passwort" required="required">
                    </div>
                    <button type="submit" class="btn btn-default">Anmelden</button>';
        if (!$kommentar == "") { echo $kommentar; }
                echo '</form></div><div class="hidden-xs col-sm-1 col-md-2 col-lg-3"></div></div></div>';
    }
    /**
     * Loginformularauswertung und Sessionlevelset
     * @param string $email
     * @param string $pass
     * @return boolean
     * @todo Medoo.php im Manager requieren
     */
    public function check($email, $pass) {
        require_once  'medoo.php';
        $database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
        $datas = $database->select("user", ["level","id","email","pass","name","prename"], ["AND" => ["email" => $email]]);
        if(count($datas) > 0) {
            foreach($datas as $data) {
                if(password_verify($pass, $data['pass'])) {
                $_SESSION['level'] = $data['level'];
                $_SESSION['id'] = $data['id'];
                $_SESSION['email'] = $data['email'];
                $_SESSION['name'] = $data['name'];
                $_SESSION['prename'] = $data['prename'];
                } else {
                    echo 'Ihr Passwort ist falsch!';
                    echo '<script type="text/javascript">
                        window.location = "?id=1002";
                        </script>';
                    return false;
                }
            }
            echo '<script type="text/javascript">
                        window.location = "?id=1002";
                        </script>';
            return true;
        } else {
            echo 'Ihr Benutzername ist falsch!';
            echo '<script type="text/javascript">
                        window.location = "?id=1002";
                        </script>';
            return false;
        }
    }
}