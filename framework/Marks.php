<?php
/**
 * Stellt die Notenverwaltung zur Verfügung
 *
 * @author Johannes
 * @todo Dummy Zustand verlassen
 */
class Marks {
    /**
     * Js includen
     */
    public function __construct() {
        $_SESSION['include'] = "dateTime";
    }
    /**
     * Schüler Notentabelle ausgeben
     */
    public function student() {
        require_once 'assets/marks/marksStudent.php';
    }
    /**
     * Lehrernotentabelle ausgeben
     */
    public function teacher() {
        require_once 'assets/marks/marksTeacher.php';
    }
}
