<?php

/**
 * 
 *
 * @author Johannes
 */
class Material {
    /**
     * 
     */
    public function __construct() {
        $_SESSION['include'] = "dateTime";
    }
    /**
     * 
     */
    public function searchForm() {
        require_once 'assets/material/search.php';
    }
    /**
     * 
     */
    public function addForm() {
        require_once 'assets/material/new.php';
    }
    /**
     * 
     */
    public function results() {
        require_once 'assets/material/result.php';
    }
    /**
     * 
     */
    public function saveEntry() {
        
    }
    /**
     * 
     */
    protected function getResults() {
        
    }
    /**
     * 
     */
    protected function moveFiles() {
        
    }
    /**
     * 
     */
    protected function checkFiles() {
        
    }
    /**
     * 
     */
    protected function deleteFiles() {
        
    }
    /**
     * 
     */
    public function deleteEntry() {
        
    }
}
