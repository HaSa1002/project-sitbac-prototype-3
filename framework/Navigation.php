<?php

/**
 * Stellt das Navigationsmenü zur Verfügung
 * 
 * @author Johannes Witt
 */
class Navigation {

    private $structure = array();
    private $out = "";
/**
 * Erstellt das Navigationsmenü
 * @param int(0-6) $level Für wen ist das Navigationsmenü?
 * @todo Fehlende Seiten auffüllen
 */
    function __construct($level = 0) {
        switch ($level) {
            case 0:
                $this->structure = array(
                    array(
                        'name' => 'Home',
                        'link' => '?id=0',
                        'childs' => array()
                    ), array(
                        'name' => 'Einloggen',
                        'link' => '?id=1',
                        'childs' => array()
                    ),array(
                        'name' => 'Registrieren',
                        'link' => '?id=2',
                        'childs' => array()
                    ), array(
                        'name' => 'Infos',
                        'link' => '?id=3',
                        'childs' => array(),
                    ), array(
                        'name' => 'Forum',
                        'link' => '#Forum',
                        'childs' => array(),
                    ), array(
                        'name' => 'Support',
                        'link' => '?id=4',
                        'childs' => array(
                            array(
                            'name' => 'Support',
                            'link' => '?id=4',
                            'childs' => array()
                            ), array(
                                'name' => 'Ideen',
                                'link' => '?id=5',
                                'childs' => array()
                            ), array(
                                'name' => 'Probleme',
                                'link' => '?id=6',
                                'childs' => array()
                            ), array(
                                'name' => 'Supportforum',
                                'link' => '#DasMussGeändertWerden',
                                'childs' => array()
                            )
                        )
                    ), array(
                        'name' => 'PhpMyAdmin',
                        'link' => '?id=1003',
                        'childs' => array()
                    )
                );
                break;
            case 1:
                break;
            case 2:
                $this->structure = array(
                    array(
                        'name' => 'Home',
                        'link' => '?id=200',
                        'childs' => array()
                    ), array(
                        'name' => 'Unterricht',
                        'link' => '?id=210',
                        'childs' => array(
                            array(
                                'name' => 'Unterricht beitreten',
                                'link' => '?id=211',
                                'childs' => array()
                            ), array(
                                'name' => 'Material',
                                'link' => '?id=216',
                                'childs' => array()
                            ), array(
                                'name' => 'Stundenplan',
                                'link' => '?id=212',
                                'childs' => array()
                            ), array(
                                'name' => 'Hausaufgaben',
                                'link' => '?id=213',
                                'childs' => array()
                            ), array(
                                'name' => 'Vertretungsplan',
                                'link' => '?id=214', //Eventuell allgemein machen
                                'childs' => array()
                            ), array(
                                'name' => 'Noten',
                                'link' => '?id=215',
                                'childs' => array()
                            )
                        )
                    ), array(
                        'name' => 'Kalender',
                        'link' => '?id=220',
                        'childs' => array()
                    ), array(
                        'name' => 'Nachrichten & Events',
                        'link' => '?id=230',
                        'childs' => array()
                    ), array(
                        'name' => 'Chat',
                        'link' => '?id=7',
                        'childs' => array()
                    ), array(
                        'name' => 'Support',
                        'link' => '?id=4',
                        'childs' => array(
                            array(
                                'name' => 'Support',
                                'link' => '?id=4',
                                'childs' => array()
                            ), array(
                                'name' => 'Ideen',
                                'link' => '?id=5',
                                'childs' => array()
                            ), array(
                                'name' => 'Probleme',
                                'link' => '?id=6',
                                'childs' => array()
                            ), array(
                                'name' => 'Supportforum',
                                'link' => '#DasMussGeändertWerden',
                                'childs' => array()
                            )
                        )
                    )
                );
                break;
            case 3:
                $this->structure = array(
                    array(
                        'name' => 'Home',
                        'link' => '?id=300',
                        'childs' => array()
                    ), array(
                        'name' => 'Unterricht',
                        'link' => '?id=310',
                        'childs' => array(
                            array(
                                'name' => 'Unterricht starten',
                                'link' => '?id=311',
                                'childs' => array()
                            ), array(
                                'name' => 'Unterricht planen',
                                'link' => '?id=312',
                                'childs' => array()
                            ), array(
                                'name' => 'Material',
                                'link' => '?id=313',
                                'childs' => array()
                            ), array(
                                'name' => 'Stundenplan',
                                'link' => '?id=314',
                                'childs' => array()
                            ), array(
                                'name' => 'Hausaufgaben',
                                'link' => '?id=315',
                                'childs' => array()
                            ), array(
                                'name' => 'Noten',
                                'link' => '?id=316',
                                'childs' => array()
                            )
                        )
                    ), array(
                        'name' => 'Schule',
                        'link' => '?id=320',
                        'childs' => array(
                            array(
                                'name' => 'Klassenbuch',
                                'link' => '?id=321',
                                'childs' => array()
                            ), array(
                                'name' => 'Nachrichten & Events',
                                'link' => '?id=322',
                                'childs' => array()
                            ), array(
                                'name' => 'Vertretungsplan',
                                'link' => '?id=323',
                                'childs' => array()
                            ), array(
                                'name' => 'Klassenbuch (alt)',
                                'link' => '?id=324',
                                'childs' => array()
                            )
                        )
                    ), array(
                        'name' => 'Kalender',
                        'link' => '?id=320',
                        'childs' => array()
                    ), array(
                        'name' => 'Chat',
                        'link' => '?id=7',
                        'childs' => array()
                    ), array(
                        'name' => 'Support',
                        'link' => '?id=4',
                        'childs' => array(
                            array(
                                'name' => 'Support',
                                'link' => '?id=4',
                                'childs' => array()
                            ), array(
                                'name' => 'Ideen',
                                'link' => '?id=5',
                                'childs' => array()
                            ), array(
                                'name' => 'Probleme',
                                'link' => '?id=6',
                                'childs' => array()
                            ), array(
                                'name' => 'Supportforum',
                                'link' => '#DasMussGeändertWerden',
                                'childs' => array()
                            )
                        )
                    )
                );
                break;
            case 4:

                break;
            case 5:

                break;
            case 6:

                break;
        }
    }
/**
 * Mit der Funktion wird das Navigationsmenüarray ausgegeben werden
 * 
 */
    public function out() {
        echo '<nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button></div>
            <div class="collapse navbar-collapse " id="bs-nav">
            <ul class="nav btn-group navbar-nav ">';
        echo $this->recursiveNaviOut($this->structure);
        if($_SESSION['level'] != 0) {
            echo '</ul><ul class="nav navbar-nav navbar-right"><li class="dropdown"><a href="?id=2000" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">'.$_SESSION['prename'].' '.$_SESSION['name'].'<span class="caret"></span></a>'.
                    '<ul class="dropdown-menu"><li><a href="?id=2000">Einstellungen</a></li><li><a href="?id=1001">Ausloggen</a></li></ul>'
                    .'</li></ul>';
        } else {
            echo '</ul><ul class="nav navbar-nav navbar-right"><li><p class="navbar-text">Nicht eingeloggt</p></li></ul>';
        }
        echo '</div></div></nav>';
    }
    /**
     * Interne Methode um Navigationsarray auszulesen
     * @param string $naviArr
     * @return string
     */
    private function recursiveNaviOut($naviArr){
        if(!empty($naviArr)){
            foreach($naviArr as $naviItem){
                if(!empty($naviItem['childs'])) {
                    $this->out .= '<li class="dropdown"><a href="' . $naviItem['link'] . '"class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $naviItem['name'] . '<span class="caret"></span></a>';
                    $this->out .= $this->childNaviOut($naviItem['childs']);
                } else {
                    $this->out .= '<li><a href="' . $naviItem['link'] . '">' . $naviItem['name'] . '</a>';
                }
                $this->out .= '</li>';
            }
        }
        return $this->out;
        
    }
    
    /**
     * Interne Methode um ChildNavigationsarray auszulesen
     * @param string $naviArr
     * 
     */
private function childNaviOut($naviArr){
        if(!empty($naviArr)){
            $this->out .= '<ul class="dropdown-menu">';
            foreach($naviArr as $naviItem){
                $this->out .= '<li><a href="' . $naviItem['link'] . '">' . $naviItem['name'] . '</a>';
                $this->out .= '</li>';
            }
            $this->out .= '</ul>';
        }
        
    }
}
