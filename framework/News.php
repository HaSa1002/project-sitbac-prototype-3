<?php
/**
 * Stellt Nachrichtendienste zur Verfügung
 *
 * @author Johannes
 */
class News {
    /**
     * 
     */
    public function __construct() {
        require_once 'medoo.php';
        $this->database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
    }
    
    /**
     * 
     */
    public function newMassage() {
        require_once 'assets/news/new.php';
    }
    /**
     * 
     */
    public function getMassages() {
        require_once 'assets/news/postbox.php';
    }
    /**
     * 
     */
    public function sendMassage() {
        
    }
    /**
     * 
     */
    public function editMassage() {
        
    }
    /**
     * 
     */
    private function send() {
        
    }
    /**
     * 
     */
    private function get() {
        
    }
    /**
     * 
     */
    private function update() {
        
    }
}
