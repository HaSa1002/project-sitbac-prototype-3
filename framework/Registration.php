<?php
/**
 * Stellt alle Loginmethoden zur Verfügung
 * 
 * @author Johannes Witt
 * @todo Passwort vergessen
 * @todo Registrierung
 */
class Registration extends Login {
    
    public function __construct() {
        $_SESSION['include'] = "reg";
    }
    /**
     * Registrationsformular
     */
    public function out() {
        include_once 'assets/registration/form.php';
        
    }
    /**
     * Loginformularauswertung und Sessionlevelset
     * @param string $email
     * @param string $pass
     * @return boolean
     * @todo Medoo.php im Manager requieren
     */
    public function check() {
        $rD = array(htmlspecialchars($_POST['prename']),htmlspecialchars($_POST['name']),htmlspecialchars($_POST['email']),htmlspecialchars($_POST['pass']));
        if(!empty($_POST['prename'] && $_POST['name'] && $_POST['email'] && $_POST['pass'] && $_POST['passR']) && ($_POST['pass'] == $_POST['passR']) && ($_POST['tat'] <= 5 && $_POST['tat'] >= 1)) {
            $rD = array(htmlspecialchars($_POST['prename']),htmlspecialchars($_POST['name']),htmlspecialchars($_POST['email']),htmlspecialchars($_POST['pass']), $_POST['tat']);
            require_once  'medoo.php';
 
            $database = new medoo([
                    // required
                    'database_type' => 'mysql',
                    'database_name' => 'elearn',
                    'server' => 'localhost',
                    'username' => 'root',
                    'password' => '',
                    'charset' => 'utf8'
                ]);
            $datas = $database->select("user", ["email"], ["AND" => ["email" => $rD[2]]]);
            if(count($datas) > 0) {
                foreach($datas as $data) {
                    $_SESSION['email'] = $data['email'];

                }
                echo '<script type="text/javascript">
                            window.location = "?id=1";
                            </script>';
                return false;
            } else {
                //Daten eintragen
                $database->insert("user", [
                    'email' => $rD[2],
                    'pass' => password_hash($rD[3], PASSWORD_DEFAULT),
                    'level' => $rD[4],
                    'prename' => $rD[0],
                    'name' => $rD[1],
                    'confirmed' => '0'
                ]);
                $this->inline("Bitte melden sie sich an. Weitere Schritte werden dann erklärt.");
            }
        } else {
            include 'assets/registration/form.php';
        }
        
        
    }
}