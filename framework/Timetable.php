<?php
/**
 * Stellt Stundenplanmethoden zur Verfügung
 *
 * @author Johannes Witt
 * @todo Wenn ein Stundenplan erstellt wurde, kann kein zweiter für die Person erstellt werden
 */
class Timetable {
    private $database;
    /**
     * DB connect
     */
    public function __construct() {
        require_once 'medoo.php';
        $this->database = new medoo([
                // required
                'database_type' => 'mysql',
                'database_name' => 'elearn',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8'
            ]);
    }
    
    /**
     * Stundenplan anzeigen
     * @todo Notices abfangen
     */
    public function drawTable() {
        $dt = $this->getData();
        if($dt) {
            echo '<div class="table-responsiv"><table class="table table-hover table-striped table-bordered">';
            echo '<tr><th>Uhrzeit</th><th>Montag</th><th>Dienstag</th><th>Mittwoch</th><th>Donnerstag</th><th>Freitag</th><th>Samstag</th><th>Sonntag</th></tr>';
            $i = 0;
            foreach ($dt as $value) {
                echo'<tr><td>'.$value['Time'].'</td><td>'.$value['Mo'].'</td><td>'.$value['Tu'].'</td><td>'.$value['We'].'</td><td>'.$value['Th'].'</td><td>'.$value['Fr'].'</td><td>'.$value['Sa'].'</td><td>'.$value['Su'].'</td></tr>';
            }    
                
            echo '</table></div>';
        } else {
            echo '<p><b>Bitte erstellen sie ihren ersten ';
            //Korrekter Link uv.
            switch ($_SESSION['level']) {
                case 2:
                    echo '<a href="?id=212">Stundenplan</a></b></p>';
                    break;
                case 3:
                    echo '<a href="?id=314">Stundenplan</a></b></p>';
                    break;
                    
            }
        }
    }
    /**
     * Stundenplaneinträge erstellen
     * @todo Daten aus DB richtig eintragen
     * @todo Dynamik
     */
    public function editTable() {
        $_SESSION['include'] = "editTable";
        echo '<h1>Stundenplan editieren (geht nicht)</h1>    
                
                <form id="timetable" method="POST">
                <div class=" form-group btn-group">
                    <button class="btn btn-default" type="button" onclick="addEditRow()">Zeile hinzufügen</button>
                    <button class="btn btn-default" type="button" onclick="deleteEditRow()"> Letzte Zeile entfernen</button>
                    <button class="btn btn-success" type="submit" name="send" value="send">Speichern</button>                    
                    <button class="btn btn-danger" type="submit" name="delete" value="delete">Löschen</button>
                    
                </div>
                <div class="table-responsiv">
                    <table class="table table-hover table-striped table-bordered">
                        <tr><th>Uhrzeit</th><th>Montag</th><th>Dienstag</th><th>Mittwoch</th><th>Donnerstag</th><th>Freitag</th><th>Samstag</th><th>Sonntag</th></tr>
                        <tbody>
                            <tr id="e1"><td><input type="text" name="edit[1][time]" class="form-control"></td><td><input type="text" name="edit[1][mo]" class="form-control"></td><td><input type="text" name="edit[1][di]" class="form-control"></td><td><input type="text" name="edit[1][mi]" class="form-control"></td><td><input type="text" name="edit[1][do]" class="form-control"></td><td><input type="text" name="edit[1][fr]" class="form-control" value=""></td><td><input type="text" name="edit[1][sa]" class="form-control"></td><td><input type="text" name="edit[1][so]" class="form-control"></td></tr>
                        </tbody>
                    </table></div>
                </form>';
        if(!empty($_POST['send'])) {
            $this->modifyData();
        } elseif(!empty($_POST['delete'])) {
            
        }
    }
    /**
     * Neuen Stundenplan erstellen
     * @todo Nur möglich, wenn kein Stundenplan vorhanden
     */
    public function createTable() {
        $_SESSION['include'] = "editTable";
        echo '<h1>Stundenplan erstellen (bitte nur einmal benutzen)</h1>    
                
                <form id="timetable" method="POST">
                <div class=" form-group btn-group">
                    <button class="btn btn-default" type="button" onclick="addRow()">Zeile hinzufügen</button>
                    <button class="btn btn-default" type="button" onclick="deleteRow()"> Letzte Zeile entfernen</button>
                    <button class="btn btn-success" type="submit" name="send" value="send">Speichern</button>
                </div>
                <div class="table-responsiv">
                    <table class="table table-hover table-striped table-bordered">
                        <tr><th>Uhrzeit</th><th>Montag</th><th>Dienstag</th><th>Mittwoch</th><th>Donnerstag</th><th>Freitag</th><th>Samstag</th><th>Sonntag</th></tr>
                        <tbody>
                            <tr id="r1"><td><input type="text" name="row[1][time]" class="form-control"></td><td><input type="text" name="row[1][mo]" class="form-control"></td><td><input type="text" name="row[1][di]" class="form-control"></td><td><input type="text" name="row[1][mi]" class="form-control"></td><td><input type="text" name="row[1][do]" class="form-control"></td><td><input type="text" name="row[1][fr]" class="form-control" value=""></td><td><input type="text" name="row[1][sa]" class="form-control"></td><td><input type="text" name="row[1][so]" class="form-control"></td></tr>
                        </tbody>
                    </table></div>
                </form>';
        if(!empty($_POST['send'])) {
            foreach($_POST['row'] as $check) {
                if(!empty($check['time'] or $check['mo'] or $check['di'] or $check['mi'] or $check['do'] or $check['fr'] or $check['sa'] or $check['so'])) {
                    $exec = TRUE;
                    break;
                } else {
                    $exec = FALSE;
                }
            }
            if($exec) {
                $this->setData();
            }
        }
    }
    /**
     * VT Plan ausgeben
     */
    public function drawVtPlan() {
        require_once 'assets/vtPlan/draw.php';
    }
    
    public function editVtPlan() {
        
    }
    public function saveVtPlan() {
        
    }
    public function createVtPlan() {
        
    }
    /**
     * 
     */
    private function getVtPlan() {
        
    }
    private function updateVtPlan() {
        
    }
    /**
     * 
     */
    /**
     * Aufgrufen von drawTable()
     * @todo Verhalten bei mehreren Stundenplänen einführen
     */
    protected function getData() {
        $gDatas = $this->database->select("timetables", ["ID","times"], ['UIDoC' => $_SESSION['id']]);
        if(!empty($gDatas)) {
            $tDatas = $this->database->select("lessontimes", ["lessonOrder","lessonTime"], ["lessonGroup" => $gDatas[0]['times'], 'ORDER' => ['lessonOrder' => 'ASC']]);
            $lDatas = $this->database->select("timetabledata", [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
            ], [
                'timetable' => $gDatas['0']['ID'],
                    'ORDER' => ['dOrder' => 'ASC']
            ]);
            $i = 0;
            foreach ($tDatas as $value) {
                $datas[$i]['Time'] = $tDatas[$i]['lessonTime'];
                $datas[$i]['Mo'] = $lDatas[$i]['Monday'];
                $datas[$i]['Tu'] = $lDatas[$i]['Tuesday'];
                $datas[$i]['We'] = $lDatas[$i]['Wednesday'];
                $datas[$i]['Th'] = $lDatas[$i]['Thursday'];
                $datas[$i]['Fr'] = $lDatas[$i]['Friday'];
                $datas[$i]['Sa'] = $lDatas[$i]['Saturday'];
                $datas[$i]['Su'] = $lDatas[$i]['Sunday'];
                $i++;
            }
        } else {
            $datas = false;
        } 
        return $datas;
    }
    /**
     * Aufgerufen von editTable()
     * @todo Modifizierungen speichern
     */
    protected function modifyData() {
        
    }
    /**
     * Aufgerufen von createTable()
     * @todo eine lessonGroup die schon da ist benutzen
     */
    protected function setData() {
        $lessonGroup = $this->database->max("lessontimes", "lessonGroup" );
        $lessonGroup++;
        $i = 0;
        foreach ($_POST['row'] as $value) {
             $this->database->insert("lessontimes", [
                'lessonGroup' => $lessonGroup,
                'lessonOrder' => $i + 1,
                'lessonTime' => $value['time']
            ]);   
            $i++;
        }
        $timetableID = $this->database->insert("timetables", [
                'UIDoC' => $_SESSION['id'],
                'times' => $lessonGroup,
            ]);
        echo 'Id des letzten Input: '.$timetableID.'<br>';
        $i = 1;
        foreach ($_POST['row'] as $val) {
            $this->database->insert("timetabledata", [
                'timetable' => $timetableID,
                'dOrder' => $i,
                'Monday' => $val['mo'],
                'Tuesday' => $val['di'],
                'Wednesday' => $val['mi'],
                'Thursday' => $val['do'],
                'Friday' => $val['fr'],
                'Saturday' => $val['sa'],
                'Sunday' => $val['so'],
            ]);
            $i++;
        }
        $error = $this->database->error();
        echo $error[0];
    }
}