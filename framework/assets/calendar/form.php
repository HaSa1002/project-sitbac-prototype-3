<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <form method="POST" action="?id=220">
            <div class="form-group">
            <label>Betreff</label><input type="text" name="subject" class="form-control">
            </div>
            <div class="form-group">
                <label>Ort</label><input type="text" name="place" class="form-control">
            </div>
            <div class="form-group form-inline"
                <p><label>Beginn</label> <input type="text" name="startDate" class="datepicker form-control"> <input type="text" name="startTime" class="timepicker form-control">
                    <label><input type="checkbox" name="allDay" onclick="f_allDay()"> Ganztägig</label></p>
                <p><label class="addon-padding-right-13">Ende</label> <input type="text" name="endDate" class="datepicker form-control"> <input type="text" name="endTime" class="timepicker form-control"></p>
            </div>
            <div class="form-group">
                <textarea name="comment" placeholder="Kommentar" class="form-control addon-calendar-textarea"></textarea>
            </div>
            <p><input type="submit" name="sub" value="Eintrag erstellen" class="btn btn-success"></p>
        </form>
    </div>
    <div class="col-lg-3">
        <h3>Sidebar</h3>
    </div>
</div>