<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h1>Klassenbuch</h1>
            <div class="form-group form-inline">
                <div class="input-group">
                    <label for="class" class="input-group-addon">Klasse:</label>
                    <select name="class" class="form-control" id="class">
                        <option value="own">Eigene Klasse</option>
                        <option value="school">Schule</option>
                        <option value="7A">7A</option>
                        <option value="7B">7B</option>
                        <option value="7C">7C</option>
                        <option value="7D">7D</option>
                        <option value="7E">7E</option>
                        <option value="7F">7F</option>
                        <option value="8A">8A</option>
                        <option value="8B">8B</option>
                        <option value="8C">8C</option>
                        <option value="8D">8D</option>
                        <option value="9A">9A</option>
                        <option value="9B">9B</option>
                        <option value="9C">9C</option>
                        <option value="9D">9D</option>
                        <option value="9E">9E</option>
                        <option value="10A">10A</option>
                        <option value="10B">10B</option>
                        <option value="10C">10C</option>
                        <option value="10D">10D</option>
                        <option value="10F">10F</option>
                    </select>
                </div>
                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" onclick="letzteWoche()"><span class="glyphicon glyphicon-menu-left"></span><span class="glyphicon glyphicon-menu-left"></span></button>
                    </span>
                    <input type="text" id="week" name="week" class="form-control datepicker">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" onclick="naechsteWoche()"><span class="glyphicon glyphicon-menu-right"></span><span class="glyphicon glyphicon-menu-right"></span></button>
                    </span>
                </div>
                <div class="btn-group">
                    <button class="btn btn-primary">Wochen</button>
                    <button class="btn btn-default">Schüler</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3><strong>2017</strong> WK 1 <small>2.1.2017 - 8.1.2017</small></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Montag</th>
                            <th>Dienstag</th>
                            <th>Mittwoch</th>
                            <th>Donnerstag</th>
                            <th>Freitag</th>
                            <th>Samstag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>Deutsch</td>
                            <td>Mathe</td>
                            <td>Englisch</td>
                            <td>Geschichte</td>
                            <td></td>
                            <td></td>
                        </tr><tr>
                            <th>2</th>
                            <td>Deutsch</td>
                            <td>Mathe</td>
                            <td>Englisch</td>
                            <td>Geschichte</td>
                            <td>Ethik</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>Mathe</td>
                            <td>Deustch</td>
                            <td>Ethik</td>
                            <td>Deutsch</td>
                            <td class="success">Englisch</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Verspätungen</th>
                            <td>Leander Hoffman <span class="badge">2</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Fehlzeiten</th>
                            <td>Pauline Kohl</td>
                            <td></td>
                            <td></td>
                            <td>Norber Lammert</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Bemerkungen</th>
                            <td>Die Klasse hat gut mitgearbeitet. (D. Lehrer)</td>
                            <td></td>
                            <td>Die Klasse wurde um den Korrekten Umgang mit Büchern belehrt. (Kl. Lehrer)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>