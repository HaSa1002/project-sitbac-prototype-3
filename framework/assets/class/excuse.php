<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-sm-10">
                <div class="container-fluid">
                    <h3>Fehlzeiten (erst OneNote!!!!!!!!)</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Anwesend</th>
                                <th>Verspätung</th>
                                <th>Bemerkung</th>
                            </tr>
                            <tr>
                                <td>Max Mustermann</td>
                                <td>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                                    <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                                <td>
                                    <form method="POST" class="form-inline">
                                        <div class="form-group">
                                             <input type="number" class="form-control"> Minuten
                                            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <textarea class="form-control"></textarea>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <td>Erich Klein</td>
                                <td>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                                    <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                                <td>
                                    <form method="POST" class="form-inline">
                                        <div class="form-group">
                                             <input type="number" class="form-control"> Minuten
                                            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <textarea class="form-control" ></textarea>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>