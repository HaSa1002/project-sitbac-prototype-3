<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <select name="class" class="form-control">
                        <option value="own">Eigene Klasse</option>
                        <option value="own">Schule</option>
                        <option value="7A">7A</option>
                        <option value="7B">7B</option>
                        <option value="7C">7C</option>
                        <option value="7D">7D</option>
                        <option value="7E">7E</option>
                        <option value="7F">7F</option>
                        <option value="8A">8A</option>
                        <option value="8B">8B</option>
                        <option value="8C">8C</option>
                        <option value="8D">8D</option>
                        <option value="9A">9A</option>
                        <option value="9B">9B</option>
                        <option value="9C">9C</option>
                        <option value="9D">9D</option>
                        <option value="9E">9E</option>
                        <option value="10A">10A</option>
                        <option value="10B">10B</option>
                        <option value="10C">10C</option>
                        <option value="10D">10D</option>
                        <option value="10F">10F</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-10">
                    <h3>Anwesenheit prüfen</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Anwesend</th>
                                <th>Verspätung</th>
                                <th>Bemerkung</th>
                            </tr>
                            <tr>
                                <td>Max Mustermann</td>
                                <td>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                                    <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                                <td>
                                    <form method="POST" class="form-inline">
                                        <div class="form-group">
                                             <input type="number" class="form-control"> Minuten
                                            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <textarea class="form-control"></textarea>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <td>Erich Klein</td>
                                <td>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                                    <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                                <td>
                                    <form method="POST" class="form-inline">
                                        <div class="form-group">
                                             <input type="number" class="form-control"> Minuten
                                            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <textarea class="form-control" ></textarea>
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                                </td>
                            </tr>

                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>