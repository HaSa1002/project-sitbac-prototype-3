<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10 col-xs-12">
            <div class="container-fluid">
                <h2>Onlinehausaufgabe hinzufügen</h2>
                <form method="POST" enctype="multipart/form-data" >
                    <label>Titel:
                        <input type="text" name="title" class="form-control">
                    </label>
                    <label>Abgabe:
                        <input type="text" name="toDate" class="form-control datepicker">
                    </label>
                    <label>Fach:
                        <input type="text" name="subject" class="form-control">
                    </label>
                    <h3>Frage hinzufügen</h3>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default">Normal</button>
                        <button type="button" class="btn btn-default">Multiple-Choice</button>
                        <button type="button" class="btn btn-default">Bild</button>
                    </div>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>