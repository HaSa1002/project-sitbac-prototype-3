<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid table-responsive">
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>Abgabe am</th>
                <th>Fach</th>
                <th>Titel</th>
                <th>Abgabe</th>
                <th>Note</th>
                <th>Aufgaben</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>31.1.2016</td>
                <td>Mathe</td>
                <td>Korrektur der 1. Klassenarbeit Mathe</td>
                <td>29.1.2016
                    <div class="btn-group">
                        <button role="button" class="btn btn-default" alt="Upload"><span class="glyphicon glyphicon-open-file"></span></button>
                        <button role="button" class="btn btn-default"> <span class="glyphicon glyphicon-save-file"></span> </button>
                    </div>
                </td>
                <td>2+</td>
                <td><button role="button" class="btn btn-default"> <span class="glyphicon glyphicon-save-file"></span> </button></td>
            </tr>
        <form method="POST">
            <tr>
                <td><input type="text" class="datepicker form-control" name="date"></td>
                <td><input type="text" class="form-control" name="subject"></td>
                <td><input type="text" class="form-control" name="title"></td>
                <td>
                    <div class="btn-group">
                        <button role="button" class="btn btn-default" alt="Upload"><span class="glyphicon glyphicon-open-file"></span></button>
                        <button role="button" class="btn btn-default"> <span class="glyphicon glyphicon-save-file"></span> </button>
                    </div>
                </td>
                <td><input type="text" class="form-control" name="mark"></td>
                <td><button type="submit" class="btn btn-success" name="save">Speichern</button></td>
                </tr>
        </form>
                
            
        </tbody>
    </table>
</div>