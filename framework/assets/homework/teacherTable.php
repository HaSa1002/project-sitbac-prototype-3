<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid table-responsive">
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>Schüler</th>
                <th>1. Klassenarbeit Deutsch<br>1.1.2017</th>
                <th><span class="js-check">Text schreiben<br>29.3.2017</span></th>
                <th><button type="button" class="btn btn-success" data-toggle="modal" data-target="#addColumn">+</button></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Max Mustermann</td>
                <td>2+</td>
                <td>N/A</td>
                <td></td>
            </tr>
            <tr>
                <td>Pauline Mustermann</td>
                <td>3-</td>
                <td>N/A</td>
                <td></td>
            </tr>
            <tr>
                <td>Frank Walter Steinhauer</td>
                <td>2</td>
                <td>N/A</td>
                <td></td>
            </tr>
            <tr>
                <td>Florian Böll</td>
                <td>2-</td>
                <td>N/A</td>
                <td></td>
            </tr>
            <tr>
                <td>Susanne Kröger</td>
                <td>1-</td>
                <td>N/A</td>
                <td></td>
            </tr>
            <tr>
                <td>Pauline Henkel</td>
                <td>1</td>
                <td>N/A</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="container-fluid table-responsive">
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr><th colspan="3">HA: Text schreiben | Bis 29.3.2017</th></tr>
            <tr>
                <th>Schüler</th>
                <th>Abgegben am</th>
                <th>Note</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Max Mustermann</td>
                <td><a href="framework/assets/homework/hw.hw" download>28.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[0]"></td>
            </tr>
            <tr>
                <td>Pauline Mustermann</td>
                <td><a href="framework/assets/homework/hw.hw" download>29.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[1]"></td>
            </tr>
            <tr>
                <td>Frank Walter Steinhauer</td>
                <td><a href="framework/assets/homework/hw.hw" download>30.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[2]"></td>
            </tr>
            <tr>
                <td>Florian Böll</td>
                <td><a href="framework/assets/homework/hw.hw" download>29.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[3]"></td>
            </tr>
            <tr>
                <td>Susanne Kröger</td>
                <td><a href="framework/assets/homework/hw.hw" download>29.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[4]"></td>
            </tr>
            <tr>
                <td>Pauline Henkel</td>
                <td><a href="framework/assets/homework/hw.hw" download>29.3.2017</a></td>
                <td><input type="text" class="form-control" name="mark[5]"></td>
            </tr>
            <tr>
                <td colspan="3"><button type="submit" class="btn btn-success">Speichern</button></td>
            </tr>
        </tbody>
    </table>
</div>




<div class="modal fade" id="addColumn" tabindex="-1" role="dialog" aria-labelledby="title">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="title">Hausaufgabe hinzufügen</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="control-label">Titel:
                            <input type="text" class="form-control" name="title">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Abgabe:
                            <input type="text" class="datepicker form-control" name="date">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Fach:
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <label>
                                        <input class="btn btn-default activ" type="checkbox" name="subject" value="Mathe">Mathe</label>
                                    </label>
                                    
                                    <button class="btn btn-default" name="subject" type="button" value="Deutsch">Deutsch</button>
                                </div>
                            <input type="text" name="type" autocomplete="off" class="form-control">
                            </div>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Beschreibung:
                            <textarea class="form-control" name="discription"></textarea>
                        </label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success">Hinzufügen</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Abbrechen</button>
            </div>
        </div>
    </div>
</div>