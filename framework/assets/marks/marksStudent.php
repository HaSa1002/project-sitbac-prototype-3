<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid table-responsive">
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>Note</th>
                <th>Titel</th>
                <th>Datum</th>
                <th>Typ</th>
                <th>mehr</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1-</td>
                <td>1. Klassenarbeit Mathe</td>
                <td>31.2.2016</td>
                <td>Klassenarbeit</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>14 P</td>
                <td>1. Klausur gmu</td>
                <td>19.4.2016</td>
                <td>Klausur</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>75 %</td>
                <td>mdl. Mitarbeit</td>
                <td>18.10.2016</td>
                <td>mündliche Mitarbeit</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>2+</td>
                <td>Erdkundetest</td>
                <td>27.1.2016</td>
                <td>Test</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>6+</td>
                <td>1. Musik LEK</td>
                <td>31.7.2016</td>
                <td>Lernerfolgskontrolle</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>63 %</td>
                <td>Präsentation Musik</td>
                <td>29.1.2016</td>
                <td>mündliche Prüfung</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            <tr>
                <td>2 P</td>
                <td>Keine Ahnung</td>
                <td>32.1.2016</td>
                <td>sonstiges</td>
                <td><button role="button" class="btn btn-default">+</button></td>
            </tr>
            
        <form method="POST">
            <tr>
                <td><input type="text" class="form-control" name="mark"></td>
                <td><input type="text" class="form-control" name="title"></td>
                <td><input type="text" class="datepicker form-control" name="date"></td>
                <td><select class="form-control" name="type"> 
                        <option value="Klassenarbeit">Klassenarbeit</option>
                        <option value="Klausur">Klausur</option>
                        <option value="mündliche Mitarbeit">mündliche Mitarbeit</option>
                        <option value="Test">Test</option>
                        <option value="Lernerfolgskontrolle">Lernerfolgskontrolle</option>
                        <option value="mündliche Prüfung">mündliche Prüfung</option>
                        <option value="sonstiges">sonstiges</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-default">Beschreibung</button> <button type="submit" class="btn btn-success" name="save">Speichern</button></td>
                </tr>
        </form>
                
            
        </tbody>
    </table>
</div>