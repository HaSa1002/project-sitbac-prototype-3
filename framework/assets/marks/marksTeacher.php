<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid table-responsive">
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>Schüler</th>
                <th>1. Klassenarbeit Deutsch</th>
                <th><button type="button" class="btn btn-success" data-toggle="modal" data-target="#addColumn">+</button></th>
                <th><button type="button" class="btn btn-success" onclick="">+</button></th>
                <th>Gesamt</th>
            </tr>
        </thead>
        <form method="POST">
            <tbody>
                <tr>
                    <td>Max Mustermann</td>
                    <td>2+</td>
                    <td><input type="text" class="form-control" name="mark[0]"></td>
                    <td></td>
                    <td>2+</td>
                </tr>
                <tr>
                    <td>Pauline Mustermann</td>
                    <td>3-</td>
                    <td><input type="text" class="form-control" name="mark[1]"></td>
                    <td></td>
                    <td>3-</td>
                </tr>
                <tr>
                    <td>Frank Walter Steinhauer</td>
                    <td>2</td>
                    <td><input type="text" class="form-control" name="mark[2]"></td>
                    <td></td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Florian Böll</td>
                    <td>2-</td>
                    <td><input type="text" class="form-control" name="mark[3]"></td>
                    <td></td>
                    <td>2-</td>
                </tr>
                <tr>
                    <td>Susanne Kröger</td>
                    <td>1-</td>
                    <td><input type="text" class="form-control" name="mark[4]"></td>
                    <td></td>
                    <td>1-</td>
                </tr>
                <tr>
                    <td>Pauline Henkel</td>
                    <td>1</td>
                    <td><input type="text" class="form-control" name="mark[5]"></td>
                    <td></td>
                    <td>1</td>
                </tr>
                <tr>
                    <td colspan="5"><button type="submit" class="btn btn-success">Speichern</button></td>
                </tr>
            </tbody>
        </form>
    </table>
</div>
<div class="modal fade" id="addColumn" tabindex="-1" role="dialog" aria-labelledby="title">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title">Benotungsspalte hinzufügen</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label class="control-label">Titel:
            <input type="text" class="form-control" name="title">
            </label>
          </div>
            <div class="form-group">
            <label class="control-label">Datum:
            <input type="text" class="datepicker form-control" name="date">
            </label>
          </div>
            <div class="form-group">
                <label>Typ:
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="type" value="mündliche Mitarbeit" autocomplete="off" checked> mündliche Mitarbeit
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="Klassenarbeit" autocomplete="off"> Klassenarbeit
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="Klausur" autocomplete="off"> Klausur
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="Test" autocomplete="off"> Test
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="LEK" autocomplete="off"> LEK
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="mündliche Prüfung" autocomplete="off"> mündliche Prüfung
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="type" value="sonstiges" autocomplete="off"> sonstiges
                        </label>
                    </div>
                </label>
          </div>
          <div class="form-group">
            <label class="control-label">Beschreibung:
            <textarea class="form-control" name="discription"></textarea>
            </label>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success">Hinzufügen</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>
  </div>
</div>