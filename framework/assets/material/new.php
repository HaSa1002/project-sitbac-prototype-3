<div class="container-fluid">
    <div class="col-xs-12">
        <h2>Eintrag erstellen</h2>
        <form method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Titel
                    <input type="text" name="title" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label>Autor
                    <input type="text" name="author" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label>Datum
                    <input type="text" name="date" class="form-control datepicker">
                </label>
            </div>
            <div class="form-group">    
                <label>Typ
                    <input type="text" name="type" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label >Lizenz
                    <input type="text" name="licence" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label>Personengruppe
                    <input type="text" name="persongroup" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label>Beschreibung
                    <textarea class="form-control" name="description"></textarea>
                </label>
            </div>
            <div class="form-group">
                <label>Datei
                    <input type="file" name="file">
                </label>
            </div>
            <div class="form-group">
                <label>Vorschau (Sorry, wir können derzeit noch keine Vorschaus selber erstellen)
                    <input type="file" name="preview">
                </label>
            </div>
            <div class="form-group btn-group">
                <button type="submit" class="btn btn-info" name="send" value="save">
                    Eintrag erstellen
                </button>
                <button type="submit" class="btn btn-warning" name="send" value="publishAtDate">
                    Eintrag erstellen und am Datum veröffentlichen
                </button>
                <button type="submit" class="btn btn-success" name="send" value="publish">
                    Eintrag erstellen und veröffentlichen
                </button>
            </div>
        </form>
    </div>
</div>