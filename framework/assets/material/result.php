<div class="container-fluid">
    <div class="col-sm-3 col-xs-12 col-lg-2">
        <p>Filtersettings o. ä.</p>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-8">
        <h2>Ergebnisse:</h2>
        <div class="container-fluid">
            <div class="row">
                
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                    <tr>
                        <td colspan="3">Tafelbild Mathematik 19.12.2018 10f</td>
                    </tr>
                    <tr>
                        <td rowspan="6">
                            <div class="media">
                                <img src="framework/assets/material/vorschau.bmp" alt="Vorschaubild" class="media-object" height="256px" width="170px">
                            </div>
                        </td>
                        <td>Autor:</td>
                        <td>K. Ahnung</td>
                    </tr>
                    <tr>
                        <td>Erstellt am:</td>
                        <td>19.12.2018</td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>Musterschule Berlin: Klasse 10f, Lehrer</td>
                    </tr>
                    <tr>
                        <td>Typ:</td>
                        <td>Tafelbild</td>
                    </tr>
                    <tr>
                        <td>Beschreibung:</td>
                        <td>Mathematiktafelbild, für alle die nicht fertig mit den Aufgaben sind!</td>
                    </tr>
                    <tr>
                        <td>Lizenz:</td>
                        <td>Keine Weiterverbreitung</td>
                    </tr>
                    <tr>
                        <td>
                            <button type="button" name="download" class="btn btn-success">Download</button>
                        </td>
                        <td>
                            <button type="button" name="import" class="btn btn-success">Importieren</button>
                        </td><td>
                            <button type="button" name="melden" class="btn btn-danger">Melden</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Tafelbild Mathematik 20.12.2018 10f</td>
                    </tr>
                    <tr>
                        <td rowspan="6">
                            <div class="media">
                                <img src="framework/assets/material/vorschau.bmp" alt="Vorschaubild" class="media-object" height="256px" width="170px">
                            </div>
                        </td>
                        <td>Autor:</td>
                        <td>K. Ahnung</td>
                    </tr>
                    <tr>
                        <td>Erstellt am:</td>
                        <td>20.12.2018</td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>Musterschule Berlin: Klasse 10f, Lehrer</td>
                    </tr>
                    <tr>
                        <td>Typ:</td>
                        <td>Tafelbild</td>
                    </tr>
                    <tr>
                        <td>Beschreibung:</td>
                        <td>Mathematiktafelbild, für alle die nicht fertig mit den Aufgaben sind!</td>
                    </tr>
                    <tr>
                        <td>Lizenz:</td>
                        <td>Keine Weiterverbreitung</td>
                    </tr>
                    <tr>
                        <td>
                            <button type="button" name="download" class="btn btn-success">Download</button>
                        </td>
                        <td>
                            <button type="button" name="import" class="btn btn-success">Importieren</button>
                        </td><td>
                            <button type="button" name="melden" class="btn btn-danger">Melden</button>
                        </td>
                    </tr>
            </table>
        </div>
    </div>
    <div class="col-sm-3 col-xs-12 col-lg-2">
        <p>Sidebar o. ä.</p>
    </div>
</div>