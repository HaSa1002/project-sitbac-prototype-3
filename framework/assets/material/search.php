<div class="container-fluid">
    <div class="col-xs-12">
        <h2>Suche</h2>
        <form method="POST" class="form-inline">
            <label>Ihre Suche:
                <input type="search" autofocus="true" class="form-control" name="search">
            </label>
            <div class="btn-group">
                <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#extendedSearch" aria-expanded="false" aria-controls="Erweiterte Suche">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </button>
                <button type="submit" class="btn btn-success" name="send" value="short">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </div>
        </form>
        <form method="POST">
            <div class="collapse well" id="extendedSearch">
                    <label class="form-group">Titel
                        <input type="text" name="title" class="form-control">
                    </label>
                    <label class="form-group">Autor
                        <input type="text" name="author" class="form-control">
                    </label>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><strong>Datum</strong></span><label class="input-group-addon">
                                <input type="radio" name="datechooser" value="before" checked="checked">Vor</label><label class="input-group-addon">
                                    <input type="radio" name="datechooser" value="after">Nach</label><input type="text" name="date" class="form-control datepicker">
                        </div>
                    </div>
                    <label class="form-group">Typ
                        <input type="text" name="type" class="form-control">
                    </label>
                    <label class="form-group">Lizenz
                        <input type="text" name="licence" class="form-control">
                    </label>
                    <label class="form-group">Personengruppe
                        <input type="text" name="persongroup" class="form-control">
                    </label>
                    <label class="form-group">Suche
                    <button type="submit" class="btn btn-success" name="send" value="extended">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </label>
            </div>
        </form>
    </div>
</div>