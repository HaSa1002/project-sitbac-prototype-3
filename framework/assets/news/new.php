<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 col-lg-6">
        <h2>Neue Nachricht</h2>
            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <select name="class" class="form-control">
                        <option value="own">Eigene Klasse</option>
                        <option value="own">Schule</option>
                        <option value="7A">7A</option>
                        <option value="7B">7B</option>
                        <option value="7C">7C</option>
                        <option value="7D">7D</option>
                        <option value="7E">7E</option>
                        <option value="7F">7F</option>
                        <option value="8A">8A</option>
                        <option value="8B">8B</option>
                        <option value="8C">8C</option>
                        <option value="8D">8D</option>
                        <option value="9A">9A</option>
                        <option value="9B">9B</option>
                        <option value="9C">9C</option>
                        <option value="9D">9D</option>
                        <option value="9E">9E</option>
                        <option value="10A">10A</option>
                        <option value="10B">10B</option>
                        <option value="10C">10C</option>
                        <option value="10D">10D</option>
                        <option value="10F">10F</option>
                    </select>
                </div>
                <div class="form-group">
                    <div data-toggle="buttons">
                        <label class="btn btn-default"> Eltern
                            <input type="checkbox" name="persons" value="parents" autoselect="off">
                        </label>
                        <label class="btn btn-default"> Schulleitung
                            <input type="checkbox" name="persons" value="schulleitung" autoselect="off">
                        </label>
                        <label class="btn btn-default"> Schüler
                            <input type="checkbox" name="persons" value="students" autoselect="off">
                        </label>
                        <label class="btn btn-default"> Schulverwaltung
                            <input type="checkbox" name="persons" value="schooladministration" autoselect="off">
                        </label>
                    </div>
                    <div class="input-group">
                        <label class="input-group-addon">
                        <input type="checkbox" aria-label="...">
                        </label>
                        <input type="text" class="form-control" placeholder="Spezielle Person (Ajax)" name="persons">
                    </div>
                </div>
                <div class="form-group">
                    <label>Titel
                        <input type="text" name="title" class="form-control">
                    </label>
                </div>
                <div class="form-group">
                    <label>Nachricht
                        <textarea class="form-control" name="message"></textarea>
                    </label>
                </div>
                <div class="form-group">
                    <input type="file" name="file">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit" name="send" value="1">Nachricht senden</button>
                </div>
            </form>
        </div>
        </div>
    <div class="col-sm-2 col-lg-4"></div>
    </div>
</div>