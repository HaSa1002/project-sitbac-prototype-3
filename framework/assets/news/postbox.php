<div class="container-fluid">
    <div class="row">
        <div class="container-fluid hidden-print">
        <h1>Posteingang <small>2 Ungelesene Nachrichten</small><button class="btn btn-default pull-right" type="button">Neue Nachricht</button></h1>
        
        </div>
        <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 hidden-print">
                <div class="container-fluid">
                    <div class="row">

                        <div class="list-group">
                            <div class="list-group-item">
                                <h3 class="list-group-item-heading">Wichtige Nachrichten <span class="badge">1</span></h3>
                            </div>
                            <a href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">Elternsprechtag <small>Kl. Lehrer</small></h4>
                                <p class="list-group-item-text">Sehr geehrte Eltern, hiermit lade ich Sie herzlich zu unserem Elternabend am 19.2.2017 ein. ...</p>
                            </a>
                            <div class="list-group-item">
                                <h3 class="list-group-item-heading">Neue Nachrichten <span class="badge">2</span></h3>
                            </div>
                            <a href="#" class="list-group-item active">
                                <h4 class="list-group-item-heading">Kafka <small>D. Lehrer</small></h4>
                                <p class="list-group-item-text">Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Böses getan hätte, ...</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">Webstandards <small>I. Lehrer</small></h4>
                                <p class="list-group-item-text">Überall dieselbe alte Leier. Das Layout ist fertig, der Text lässt auf sich warten. ...</p>
                            </a>
                            <div class="list-group-item">
                                <h3 class="list-group-item-heading">Ältere Nachrichten <span class="badge">2</span></h3>
                            </div>
                            <a href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">Updateinformationen <small>System</small></h4>
                                <p class="list-group-item-text">Hallo, heute ist wieder eine neues Update erschienen. Bitte beachten Sie, dass sich folgende ...</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <h4 class="list-group-item-heading">Willkommen bei SITBAC <small>System</small></h4>
                                <p class="list-group-item-text">Willkommen bei SITBAC. Wir freuen uns Sie hier begrüßen zu dürfen. Bitte nehmen Sie ...</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="container-fluid">
                    <div class="row panel panel-default">
                        <div class="panel-heading addon-padding-down-0">
                            <p>Betreff: <b>Kafka</b></p>
                        </div>
                        <div class="panel-heading addon-padding-down-0">
                            <p>Von: <b>D. Lehrer</b></p>
                        </div>
                        <div class="panel-heading addon-padding-down-0">
                            <p>An: <b>Klasse 10D (Schüler), Romain-Rolland-Gymnasium Berlin</b></p>
                        </div>
                        <div class="panel-heading addon-padding-down-0 addon-padding-top-0 hidden-print">
                            <div class="btn-group panel-body">
                                <a class="btn btn-success btn-sm" href="framework/assets/news/Kafka.docx">Kafka.docx</a>
                                <button type="button" class="btn btn-primary btn-sm">Antworten</button>
                                <button type="button" class="btn btn-info btn-sm">Allen Antworten</button>
                                <button type="button" class="btn btn-info btn-sm">Weiterleiten</button>
                                <button type="button" class="btn btn-warning btn-sm">Als Wichtig markieren</button>
                                <button type="button" class="btn btn-danger btn-sm">Löschen</button>
                            </div>
                        </div>
                        <div class="panel-body">Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Böses getan hätte, wurde er eines Morgens verhaftet. »Wie ein Hund! « sagte er, es war, als sollte die Scham ihn überleben. Als Gregor Samsa eines Morgens aus unruhigen Träumen erwachte, fand er sich in seinem Bett zu einem ungeheueren Ungeziefer verwandelt. Und es war ihnen wie eine Bestätigung ihrer neuen Träume und guten Absichten, als am Ziele ihrer Fahrt die Tochter als erste sich erhob und ihren jungen Körper dehnte. »Es ist ein eigentümlicher Apparat«, sagte der Offizier zu dem Forschungsreisenden und überblickte mit einem gewissermaßen bewundernden Blick den ihm doch wohlbekannten Apparat. Sie hätten noch ins Boot springen können, aber der Reisende hob ein schweres, geknotetes Tau vom Boden, drohte ihnen damit und hielt sie dadurch von dem Sprunge ab. In den letzten Jahrzehnten ist das Interesse an Hungerkünstlern sehr zurückgegangen.
                            Aber sie überwanden sich, umdrängten den Käfig und wollten sich gar nicht fortrühren. 
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-2 hidden-xs hidden-print">
                    <p>Sidbar (rechts) 1 2 3 4 5 6 7 8 9 10 11 12 13</p>
            </div>
        </div>
    </div>
    </div>
</div>