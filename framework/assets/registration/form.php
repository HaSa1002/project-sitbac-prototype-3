<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid">
        <div class="row">
            <div class="hidden-xs col-sm-1 col-md-2 col-lg-3"></div>
            <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
                <h1>Registration</h1>
            <form method="POST" action="?id=1005">
                    <div class="form-group">

                        <label for="prename">Vorname</label>
                        <input type="text" class="form-control" id="prename" name="prename" placeholder="Vorname" required="required"
                               <?php if(!empty($rD[0])){echo"value=\"$rD[0]\"";}?>
                    ></div>
                    <div class="form-group">

                        <label for="name">Nachname</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nachname" required="required"
                                <?php if(!empty($rD[1])){echo"value=\"$rD[1]\"";}?>
                               >
                    </div>
                <div>
                    <label>Tätigkeit
                    <div class="btn-group" data-toggle="buttons">
                            
                      <label class="btn btn-default active">
                          <input type="radio" name="tat" value="1" autocomplete="off" checked> Eltern
                      </label>
                      <label class="btn btn-default">
                        <input type="radio" name="tat" value="2" autocomplete="off"> Schüler
                      </label>
                        <label class="btn btn-default" data-toggle="popover" data-trigger="focus" data-title="Achtung: Händische Verifikation!" data-placement="top" data-content="Aufgrund der notwendigen händischen Verifikation kann es zu längeren Freischaltzeiten kommen!">
                        <input type="radio" name="tat" value="3" autocomplete="off" data-target="#hinweis"> Lehrer
                      </label>
                        <label class="btn btn-default" data-toggle="popover" data-trigger="focus" data-title="Achtung: Händische Verifikation!" data-placement="top" data-content="Aufgrund der notwendigen händischen Verifikation kann es zu längeren Freischaltzeiten kommen!">
                        <input type="radio" name="tat" value="4" autocomplete="off" data-target="#hinweis"> Schulleitung
                      </label>
                        <label class="btn btn-default" data-container="body" data-toggle="popover" data-title="Achtung: Händische Verifikation!" data-placement="top" data-trigger="focus" data-content="Aufgrund der notwendigen händischen Verifikation kann es zu längeren Freischaltzeiten kommen!">
                            <input type="radio" name="tat" value="5" autocomplete="off" > Schulverwaltung
                      </label>
                        
                    </div>
                    </label>
                    <div class="form-group">
                        <label for="school">Schule</label>
                            <input type="text" class="form-control" id="school" name="school" placeholder="Schule Derzeit ohne Funktion" disabled="true">
                             
                        
                    </div>
                    <div class="form-group">

                        <label for="email">E-Mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail" required="required"
    <?php if(!empty($rD[2])){echo"value=\"$rD[2]\"";}?>
>
                    </div>
                    <div class="form-group">
                        <label for="pass">Passwort</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Passwort" required="required"
    <?php if(!empty($rD[3])){echo"value=\"$rD[3]\"";}?>
>
                    </div>
                    <div class="form-group">
                        <label for="passR">Passwort wiederholen</label>
                        <input type="password" class="form-control" id="passR" name="passR" placeholder="Passwort wiederholen" required="required">
                    </div>
                <div><button type="submit" class="btn btn-default">Registrieren</button></div>
                <div><p> Mit dem klicken auf diesen Button bestätigen sie,
                        das sie unsere <a href="#">Nutzungsbedingungen</a>, <a href="#">AGBs</a> und unsere <a href="#">Datenschutzerklärung</a> gelesen haben und ihnen zustimmen.
                    Das ganze soll es dann nochmal übersichtlich und transparent geben.</p></div>
            </div></form><div class="hidden-xs col-sm-1 col-md-2 col-lg-3">  
            </div>       
        </div>       
</div>
</div>
