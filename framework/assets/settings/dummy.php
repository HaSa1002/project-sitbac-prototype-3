<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="container-fluid">
                <h1>Einstellungen (Dummy)</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="container-fluid">
                <h2>Über mich...</h2>
                <form method="POST">
                    <div class="form-group">
                        <label>Vorname, Name</label>
                            <input type="text" class="form-control" name="prename" value="Vorname des Benutzers">
                            <input type="text" class="form-control" name="name" value="Nachname des Benutzers">
                        
                    </div>
                    <div class="form-group">
                        <label>Schule, Klasse</label>
                            <input type="text" class="form-control" name="school" placeholder="" value="Schule des Benutzers">
                            <input type="text" class="form-control" name="class" value="Klasse des Benutzers">
                        
                    </div>
                    <div class="form-group">
                        <label>E-Mail</label>
                            <input type="text" class="form-control" name="oldEMail" placeholder="Alte E-Mail Adresse">
                            <input type="text" class="form-control" name="newEMail" placeholder="Neue E-Mail Adresse">
                        
                    </div>
                    <div class="form-group">
                        <label for="pass">Passwort</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Passwort" required="required">
                    </div>
                    <div class="form-group">
                        <label for="passR">Passwort wiederholen</label>
                        <input type="password" class="form-control" id="passR" name="passR" placeholder="Passwort wiederholen" required="required">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>