<?php
if (!defined('OK')) {
    die('<h1>403 Forbidden</h1><p>Sie sollten das lassen!</p>');
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="hidden-xs col-sm-1 col-md-2 col-lg-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
            <h1>Schülerstartseite</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7 col-md-8 table-responsive">
            <?php
            include_once 'framework/Timetable.php';
                $timetable = new Timetable();
                $timetable->drawTable();
            ?>
        </div>
        <div class="col-lg-5 col-md-4 table-responsive">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr><th colspan="3">Heute</th></tr>
                <tr><th>Uhrzeit</th><th>Betreff</th><th>Ort</th></tr>
                </thead>
                <tbody>
                    <tr><td>8:00</td><td>Sitzung</td><td>Plenum</td></tr>
                    <tr><td>10:30</td><td>Themendisskusion</td><td>Fernseher</td></tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-5 col-md-4 table-responsive">
            <p>Überall dieselbe alte Leier. Das Layout ist fertig, der Text lässt auf sich warten. Damit das Layout nun nicht nackt im Raume steht und sich klein und leer vorkommt, springe ich ein: der Blindtext. Genau zu diesem Zwecke erschaffen, immer im Schatten meines großen Bruders »Lorem Ipsum«, freue ich mich jedes Mal, wenn Sie ein paar Zeilen lesen. Denn esse est percipi - Sein ist wahrgenommen werden. Und weil Sie nun schon die Güte haben, mich ein paar weitere Sätze lang zu begleiten, möchte ich diese Gelegenheit nutzen, Ihnen nicht nur als Lückenfüller zu dienen, sondern auf etwas hinzuweisen, das es ebenso verdient wahrgenommen zu werden: Webstandards nämlich. Sehen Sie, Webstandards sind das Regelwerk, auf dem Webseiten aufbauen. So gibt es Regeln für HTML, CSS, JavaScript oder auch XML; Worte, die Sie vielleicht schon einmal von Ihrem Entwickler gehört haben. Diese Standards sorgen dafür, dass alle Beteiligten aus einer Webseite den größten Nutzen ziehen. Im Gegensatz zu früheren Webseiten müssen wir zum Beispiel nicht mehr zwei verschiedene Webseiten für den Internet Explorer und einen anderen Browser programmieren. Es reicht eine Seite, die - richtig angelegt - sowohl auf verschiedenen Browsern im Netz funktioniert, aber ebenso gut für den Ausdruck oder die Darstellung auf einem Handy geeignet ist. Wohlgemerkt: Eine Seite für alle Formate. Was für eine Erleichterung. Standards sparen Zeit bei den Entwicklungskosten und sorgen dafür, dass sich Webseiten später leichter pflegen lassen. Natürlich nur dann, wenn sich alle an diese Standards halten. Das gilt für Browser wie Firefox, Opera, Safari und den Internet Explorer ebenso wie für die Darstellung in Handys. Und was können Sie für Standards tun? Fordern Sie von Ihren Designern und Programmieren einfach standardkonforme Webseiten. Ihr Budget wird es Ihnen auf Dauer danken. Ebenso möchte ich Ihnen dafür danken, dass Sie mich bin zum Ende gelesen haben. Meine Mission ist erfüllt. Ich werde hier noch die Stellung halten, bis der geplante Text eintrifft. Ich wünsche Ihnen noch einen schönen Tag. Und arbeiten Sie nicht zuviel!</p>
            </table>
        </div>
    </div>
</div>



<?php
//Module etc.
?>