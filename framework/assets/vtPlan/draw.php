<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h2>Vertretungsplan <small>für <b>Mittwoch, den 25.1.2017</b></small></h2>
            <select name="class" class="form-control">
                <option value="own">Eigene Klasse</option>
                <option value="own">Schule</option>
                <option value="7A">7A</option>
                <option value="7B">7B</option>
                <option value="7C">7C</option>
                <option value="7D">7D</option>
                <option value="7E">7E</option>
                <option value="7F">7F</option>
                <option value="8A">8A</option>
                <option value="8B">8B</option>
                <option value="8C">8C</option>
                <option value="8D">8D</option>
                <option value="9A">9A</option>
                <option value="9B">9B</option>
                <option value="9C">9C</option>
                <option value="9D">9D</option>
                <option value="9E">9E</option>
                <option value="10A">10A</option>
                <option value="10B">10B</option>
                <option value="10C">10C</option>
                <option value="10D">10D</option>
                <option value="10F">10F</option>
            </select>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <th>Klasse</th>
                        <th>Stunde</th>
                        <th>Vertretungslehrer</th>
                        <th>Vertretungsfach</th>
                        <th>Vertretungsraum</th>
                        <th>Bemerkungen</th>
                    </tr>
                    <tr>
                        <td>9c</td>
                        <td>1</td>
                        <td>*frei</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9c</td>
                        <td>2</td>
                        <td>Fahr</td>
                        <td>Deutsch</td>
                        <td>29</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9c</td>
                        <td>3</td>
                        <td>Inglisch</td>
                        <td>Englisch</td>
                        <td></td>
                        <td>anstatt Mi. (25.1.2017) 7. Stunde</td>
                    </tr>
                    <tr>
                        <td>9c</td>
                        <td>7</td>
                        <td>*frei</td>
                        <td></td>
                        <td></td>
                        <td>verschoben auf Mi. (25.1.2017) 3. Stunde</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>