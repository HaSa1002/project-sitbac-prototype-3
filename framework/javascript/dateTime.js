jQuery(function($){
$.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
    closeText: 'Schließen', closeStatus: 'ohne Änderungen schließen',
    prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
    nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
    currentText: 'Heute', currentStatus: '',
    monthNames: ['Januar','Februar','März','April','Mai','Juni',
    'Juli','August','September','Oktober','November','Dezember'],
    monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
    'Jul','Aug','Sep','Okt','Nov','Dez'],
    monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
    weekHeader: 'Wo', weekStatus: 'Woche des Monats',
    dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
    dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
    dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
    dateFormat: 'dd.mm.yy', firstDay: 1, 
    initStatus: 'Wähle ein Datum', isRTL: false};
$.datepicker.setDefaults($.datepicker.regional['de']);
});
var $j = jQuery.noConflict();
$j(".datepicker").datepicker({
    showButtonPanel: true
});
var $t = jQuery.noConflict();
$t(document).ready(function(){
    $j('input.timepicker').timepicker({});
});
$t('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '00:00',
    maxTime: '23:59',
    defaultTime: '00:00',
    startTime: '00:00',
    dynamic: true,
    dropdown: true,
    scrollbar: true
});
function f_allDay() {
        if( jQuery("input[name='allDay']").is(":checked")) {
            jQuery(".timepicker").attr("disabled", "");
        } else {
            jQuery(".timepicker").removeAttr("disabled");
        }
}