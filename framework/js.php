<?php
/**
 * JS includes
 */

//Immer gebraucht

//JQuery
echo '<script src="framework/thirdParty/jquery-3.1.1.js" type="text/javascript"></script>'; 
//evtl. <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

//Bootstrap JS
echo '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>';

if(isset($_SESSION['include'])) {
    switch ($_SESSION['include']) {
        case "dateTime":
            echo '<script src="framework/thirdParty/jQueryUI/jquery-ui.js" type="text/javascript"></script>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <link href="framework/thirdParty/timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css"/>
            <script src="framework/thirdParty/timepicker/jquery.timepicker.js" type="text/javascript"></script>
            <script src="framework/javascript/dateTime.js" type="text/javascript"></script>';
            break;
        case "dateWK":
            echo '<script src="framework/thirdParty/jQueryUI/jquery-ui.js" type="text/javascript"></script>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
            <script src="framework/javascript/datepickerWK.js" type="text/javascript"></script>';
            break;
        case "reg":
            echo '<script src="framework/assets/registration/popover.js" type="text/javascript"></script>';
            break;
        case "editTable":
            echo '<script src="framework/javascript/editTimetable.js" type="text/javascript"></script>';
            break;
    }
}