<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="framework/addon.css" rel="stylesheet" type="text/css"/>    
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            
        <title>SITBAC</title>
    </head>
    <body>
        <noscript>
        Warnung! Diese Seite benutzt JavaScript. Sollten sie JavaScript deaktiviert haben, aktivieren sie es bitte. Ein Entsprechendes Guide kann 
        <a href="#">hier</a> gefunden werden. Sollten sie diese Meldung für einen Fehler halten oder keine Lösung für dieses Problem finden,
        wenden sie sich bitte an den <a href="#">Support</a>.
        </noscript>     
<?php
    //Sicherheitskonstante für includes
    define('OK','OK');
    //Sessionverwaltung starten/überprüfen
    session_start();
    if(empty($_SESSION['level'])) {
        $_SESSION['level'] = 0;
    }
    $_SESSION['include'] = "";
    if(!isset($_GET['id'])) {
        $_GET['id'] = 0;
    }

    //Debugvariablen
    //auf false setzen, wenn nicht erwünscht
    $debugMode = false;
    if($debugMode){
    print_r($_SESSION);
    echo $_GET['id'];
    }
    //Framework einbinden
    require_once('framework/navigation.php');
    require_once ('framework/medoo.php');
    require_once 'framework/Infos.php';
    require_once 'framework/Content.php';
    $inf = new Infos();
    $Content = new Content();
    $Navigation = new navigation($_SESSION['level']);
    switch ($_SESSION['level']) {
        case 0: 
            //Objektdeklarationen
            require_once('framework/Login.php');
            $Navigation->out();
            echo '<div class=container-fluid>';
            $loginForm = new Login();
            
            //ganz unten
            switch($_GET['id']) {
                case 0: //Home
                    $loginForm->inline();
                    $Content->home();
                    break;
                case 1: //Anmelden
                    $loginForm->out();
                    break;
                case 2: //Registrieren
                    require_once 'framework/Registration.php';
                    $reg = new Registration();
                    $reg->out();
                    break;
                case 3: //Infos
                    break;
                case 4: //Support
                    break;
                case 5: //Ideen
                    break;
                case 6: //Probleme
                    break;
                case 1000: //Anmeldeformular
                    if(!empty($_POST['email'] & $_POST['pass'])) {
                    $loginForm->check($_POST['email'], $_POST['pass']);
                    } else {
                        echo '<script type="text/javascript">
                            window.location = "?id=1";
                            </script>';
                        echo 'Bitte sowohl E-Mail als auch Passwort eintragen.';
                    }
                    if($debugMode){print_r($_POST);}
                    break;
                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        case 1:
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {

                default:
                    staticCheck($_GET['id']);
                    break;
            }
            break;
        case 2:
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {
                case 200:
                    require_once 'framework/assets/students/home.php';
                    break;
                case 210:
                    staticCheck($_GET['id']);
                    break;
                case 211:
                    require_once 'framework/Lesson.php';
                    $Lesson = new Lesson();
                    $Lesson->warning();
                    break;
                case 212:
                    echo '<div class="container-fluid">';
                    require_once 'framework/Timetable.php';
                    $Timetable = new Timetable();
                    $Timetable->drawTable();
                    $Timetable->editTable();
                    $Timetable->createTable();
                    echo '</div>';
                    break;
                case 213:
                    require_once 'framework/Homework.php';
                    $Homework = new Homework();
                    $Homework->homeworkTable();
                        
                    break;
                case 214:
                    require_once 'framework/Timetable.php';
                    $Timetable = new Timetable();
                    $Timetable->drawVtPlan();
                    break;
                case 215:
                    require_once 'framework/Marks.php';
                    $Marks = new Marks();
                    $Marks->student();
                    break;
                case 216:
                    require_once 'framework/Material.php';
                    $Material = new Material();
                    $Material->searchForm();
                    $Material->results();
                    $Material->addForm();
                    break;
                case 220:
                    require_once 'framework/Calendar.php';
                    $Cal = new Calendar();
                    $Cal->send();
                    $Cal->form($_SESSION['message'][0],$_SESSION['message'][1]);
                    $Cal->draw();
                    break;
                case 230;
                    require_once 'framework/News.php';
                    $News = new News();
                    $News->getMassages();
                    $News->newMassage();
                    break;
                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        case 3:
            
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {
                case 300:
                    require_once 'framework/assets/teachers/home.php';
                    break;
                case 311:
                case 312:
                    require_once 'framework/Lesson.php';
                    Lesson::warning();
                    break;
                case 313:
                    require_once 'framework/Material.php';
                    $Material = new Material();
                    $Material->searchForm();
                    $Material->results();
                    $Material->addForm();
                    break;
                case 315:
                    require_once 'framework/Homework.php';
                    $Homework = new Homework();
                    $Homework->homeworkTable();
                    break;
                case 314:
                    require_once 'framework/Timetable.php';
                    $Timetable = new Timetable();
                    echo '<div class="container-fluid">';
                    $Timetable->drawTable();
                    $Timetable->editTable();
                    $Timetable->createTable();
                    echo '</div>';
                    break;
                case 316:
                    require_once 'framework/Marks.php';
                    $Marks = new Marks();
                    $Marks->teacher();
                    break;
                case 320:
                    require_once 'framework/Calendar.php';
                    $Cal = new Calendar();
                    $Cal->send();
                    $Cal->form($_SESSION['message'][0],$_SESSION['message'][1]);
                    $Cal->draw();
                    break;
                case 321:
                    require_once 'framework/ClassManager.php';
                    $Class = new ClassManager();
                    $Class->classbook();
                    break;
                case 322:
                    require_once 'framework/News.php';
                    $News = new News();
                    $News->getMassages();
                    $News->newMassage();
                    
                    break;
                case 323:
                    require_once 'framework/Timetable.php';
                    $Timetable = new Timetable();
                    $Timetable->drawVtPlan();
                    break;
                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        case 4:
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {
                case 400:
                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        case 5:
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {

                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        case 6:
            
            $Navigation->out();
            echo '<div class=container-fluid>';
            switch ($_GET['id']) {
                case 600:
                    
                    break;
                default:
                    staticCheck($_GET['id']);
                    break;
            }
            echo '</div>';
            break;
        default:
            staticCheck($_GET['id']);
            break;
    }
    //Debug
    if($debugMode){
    print_r($_SESSION);
    echo $_GET['id'];
    }
    ?>
        <footer class="footer container-fluid navbar-default">
            <p class="navbar-text">Version: <?php $inf->version();?> <?php $inf->hinweis(); ?></p>
        </footer>
    
    <?php
//Hier hinter kommen nur die Scriptincludes als LETZTES
    require_once 'framework/js.php';
/**
 * Wird aufgerufen wenn der HauptSwitch in den default wechselt;
 * @param int $id Seitenparameter
 */
function staticCheck($id) {
    switch ($id) {
        case 7: //Chat
            require_once 'framework/Chat.php';
            Chat::warning();
            break;
        case 4: //Support
            break;
        case 5: //Ideen
            break;
        case 6: //Probleme
            break;
        case 1001: //Ausloggen
            session_destroy();
            echo '<script type="text/javascript">
                        window.location = "?id=0";
                        </script>';
            break;
        case 1003:
                echo'<script type="text/javascript">
                        window.location = "http://localhost/phpmyadmin/";
                        </script>';
                                break;
        case 1005:
            require_once 'framework/Registration.php';
            $reg = new Registration;
            $reg->check();
            break;
        case 2000:
            require_once 'framework/Settings.php';
            $Settings = new Settings();
            $Settings->drawDummy();
            break;
        default:
            switch ($_SESSION['level']) {
                case 0:
                    echo '<script type="text/javascript">
                        window.location = "?id=0";
                        </script>';
                    break;
                case 1:
                    echo '<script type="text/javascript">
                        window.location = "?id=100";
                        </script>';
                    break;
                case 2:
                    echo '<script type="text/javascript">
                        window.location = "?id=200";
                        </script>';
                    break;
                case 3:
                    echo '<script type="text/javascript">
                        window.location = "?id=300";
                        </script>';
                    break;
                case 4:
                    echo '<script type="text/javascript">
                        window.location = "?id=400";
                        </script>';
                    break;
                case  5:
                    echo '<script type="text/javascript">
                        window.location = "?id=500";
                        </script>';
                    break;
                case 6:
                    echo '<script type="text/javascript">
                        window.location = "?id=600";
                        </script>';
                    break;
                default:
                    die('Uuups! Das darf nicht passieren! Geloggt und demnächst gefixt! Bitte versuche deinen Browser zu beenden und die Seite erneut aufzurufen!');
            }
            break;
    }
}
?>
        
    </body>
</html>
